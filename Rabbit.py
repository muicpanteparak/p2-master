import pika
import json
import time
from logger import get_logger
logging = get_logger()

# from flask.ext.pika import Pika as FPika

"""A rabbit mq wrapper"""


def retry_on_failure(on_failure):
    def real_decorator(function):
        def wrapper(*args, **kwargs):
            result = function(*args, **kwargs)
            return result

        return wrapper

    return real_decorator


class Rabbit(object):
    """A rabbit mq wrapper"""

    MAX_RETRY = 10

    def __init__(self, host="localhost", port=5672, username="guest", password="guest", no_ack: bool = False):
        self.parameters = pika.ConnectionParameters(
            host=host,
            port=port,
            heartbeat=0,
            credentials=pika.PlainCredentials(
                username=username,
                password=password
            ))
        self.conn = self.connect()  # , credentials=pika.PlainCredentials(username=username, password=password)
        self.chan = None
        self.no_ack = no_ack

    def connect(self):
        self.conn = pika.BlockingConnection(self.parameters)
        return self.conn

    def is_connection_closed(self):
        return self.conn is None or self.conn.is_closed

    def is_channel_closed(self):
        return self.chan is None or self.chan.is_closed

    def get_channel(self):
        retry = 0
        while retry < self.MAX_RETRY:
            try:
                # channel = self.conn.channel()
                # return channel
                if self.is_connection_closed():
                    self.connect()
                    self.chan = self.conn.channel()
                    logging.debug("Connection closed, attempt reconnect")
                    return self.chan

                if self.is_channel_closed():
                    logging.debug("Channel closed, recreating")
                    self.chan = self.conn.channel()

                logging.debug("pass connection and channel checks")
                return self.chan
            except pika.exceptions.ConnectionClosed as e:
                # LOG.info('oops. lost connection. trying to reconnect.')
                logging.warning('oops. lost connection. trying to reconnect.', retry)
                logging.warning(e)
                time.sleep(0.5)
                self.conn = self.connect()
                retry += 1

        if retry >= 5:
            logging.warning("Maximum reconnect exceeded")
            # LOG.info("Maximum reconnect exceeded")

    def declare_queue(self, qn: str, durable: bool = True, exclusive: bool = False, auto_delete: bool = False):
        if not self.is_queue_name_valid(qn):
            raise RuntimeError(f"Invalid Queue Name: ${qn}")

        self.get_channel().queue_declare(queue=qn, durable=durable, exclusive=exclusive, auto_delete=auto_delete)

    def publish(self,
                msg,
                qn: str):
        self.declare_queue(qn)
        self.get_channel().basic_publish("", qn, json.dumps(msg))

    @staticmethod
    def is_queue_name_valid(qn: str):
        return not qn.startswith("amq.")

    def listen(self, qn: str, cb):
        def wrapper(*args, **kwargs):
            # ch, method, properties, body
            ch, method, _, body = args

            try:
                args = ch, method, _, json.loads(body.decode('utf-8'))
                cb(*args, **kwargs)
            except Exception as e:
                ch.basic_nack(delivery_tag=method.delivery_tag)
                logging.debug(e)

            ch.basic_ack(delivery_tag=method.delivery_tag)

        self.declare_queue(qn)
        self.get_channel().basic_consume(wrapper, queue=qn, no_ack=self.no_ack)
        logging.info("Start consuming")
        self.get_channel().start_consuming()

    def exchange_declare(self, exchange_name, exchange_type: str = "fanout", durable: bool = True):
        if not self.is_queue_name_valid(exchange_name):
            raise RuntimeError("Invalid Exchange Name")

        self.get_channel().exchange_declare(exchange_name, exchange_type=exchange_type, durable=durable)

    def publish_to_exchange(self, msg, exchange_name: str):
        self.exchange_declare(exchange_name)
        self.get_channel().basic_publish(exchange=exchange_name, routing_key="", body=msg)

    def listen_to_exchange(self, exchange_name: str, cb, queue_name=None):
        if queue_name is None:
            queue_name = self.get_channel().queue_declare().method.queue

        self.declare_queue(queue_name)
        self.get_channel().queue_bind(queue=queue_name, exchange=exchange_name)

        def wrapper(*args, **kwargs):
            _, method, __, ___ = args
            try:
                cb(*args, **kwargs)
            except Exception as e:
                method.basic_nack(delivery_tag=method.delivery_tag)
                logging.warning(e)
            method.basic_ack(delivery_tag=method.delivery_tag)

        self.get_channel().basic_consume(wrapper, queue=queue_name, routing=None, no_ack=self.no_ack)
        self.get_channel().start_consuming()

    def close(self):
        if self.conn is not None or not self.is_connection_closed():
            self.conn.close()
