import redis

from flatten_json import flatten, unflatten_list


class Redis(object):
    def __init__(self, host="localhost", port=6379, password=None):
        self.host = host
        self.port = port
        self.password = password
        self.conn = self.connect()

    def connect(self):
        self.conn = redis.StrictRedis(
            host=self.host,
            port=self.port, db=0,
            password=self.password,
            decode_responses=True,
            socket_keepalive=True,
            retry_on_timeout=True
        )
        return self.conn

    def get_connection(self):
        return self.conn

    def put(self, name, dictionary):
        return self.get_connection().hmset(name, flatten(dictionary, "/"))

    def get(self, name, key):
        val = self.get_connection().hmget(name, key)
        return val[0] if len(val) == 1 else (None if len(val) == 0 else val)

    def get_by_prefix(self, name: str, prefix_key: str):
        matcher = (prefix_key if prefix_key.endswith("*") else prefix_key + "*")
        out = self.get_connection().hscan_iter(name, match=matcher)
        return unflatten_list(dict(out), "/")

    # def parsers(self, data):
    #
    #     def parse(val):
    #         low = val.lower()
    #         if val in ["false", "true"]:
    #             return {"false": False, "true": True, "None": None}.get(low)
    #         else:
    #             return val
    #
    #     for (k, v) in data:
    #         yield k, parse(v)

    def delete(self, name, key):
        self.get_connection().hdel(name, key)

    def list(self, name):
        return self.get_connection().hkeys(name)


# my_dict = {"abc": "def", "ab": [1, 2, 3, None], "aaa1": {"a12d": "bhd", "a1230": {"abvw": None, "kea": [{"a": "b", "bc": "dea"}, {"2ja": "yplo","cdla": "yooooo"}]}}}
# t = [{'a': '1', 'b': '2', 'c_d': '3', 'c_e': '4'}, {'a': '0.5', 'c_d': '3.2'}, {'a': '0.8', 'b': '1.8'}]


# print(flatten(my_dict))
# print(unflatten_list({'abc': 'def', 'ab/0': '1', 'ab/1': '2', 'ab/2': '3', 'ab/3': 'None'}))
# print(flatten_json(t))
# print(nested_to_record(my_dict, sep='/'))

# r = Redis()
# name = "abc"
# r.put(name, {"extract_complete": False})
# print(r.get("abc", "extract_complete") is False)
# # print(unflatten_list(dict(r.get_by_prefix(name, "ab/")), "/"))
# r.delete(name, "abc")
# print(r.get_by_prefix(name, "aaa1/a1230/*"))
