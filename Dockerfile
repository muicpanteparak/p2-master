FROM python:3.7
WORKDIR /app
ARG MINIO_URL
ARG REDIS_URL
ARG WAIT_FOR_TIMEOUT

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh wait-for-it
RUN chmod +x wait-for-it
#CMD python3 -u app.py
#CMD "./wait-for-it ${MINIO_URL} -s -q -t ${WAIT_FOR_TIMEOUT} -- wait-for-it ${REDIS_URL} -s -q -t ${WAIT_FOR_TIMEOUT} -- python3 app.py"
CMD gunicorn -w 1 -b 0.0.0.0:5000 app:app

