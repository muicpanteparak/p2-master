# from gevent import monkey;
#
# monkey.patch_socket()
from Redis import Redis
from flask import Flask, request, jsonify
from flask_socketio import SocketIO, join_room, leave_room
from Rabbit import Rabbit
import os
import sys
from logger import get_logger

logging = get_logger()

EXPOSE_PORT = int(os.getenv("PORT", 5000))

RABBIT_HOST = os.getenv("MY_RABBIT_HOST", "localhost")
RABBIT_PORT = int(os.getenv("MY_RABBIT_PORT", 5672))
RABBIT_USER = os.getenv("MY_RABBIT_USER", "guest")
RABBIT_PASS = os.getenv("MY_RABBIT_PASS", "guest")

REDIS_HOST = os.getenv("MY_REDIS_HOST", "localhost")
REDIS_PORT = int(os.getenv("MY_REDIS_PORT", 6379))
REDIS_PASS = os.getenv("MY_REDIS_PASS", None)

app = Flask(__name__)
# app.config['APPLICATION_ROOT'] = 'api'
socketio = SocketIO(app, async_mode='threading')
thread = None

STATUS = ["extract", "parse", 'parse_complete', 'extract_complete', 'compression_complete', "converted", "extracted",
          "compression", "extract_complete", "compress_complete", "extract_complete", "parse_complete",
          "compression_complete"]


@app.route("/health", methods=["GET"])
def health():
    return jsonify({})


@app.route("/", methods=["GET"])
def health_ingress():
    return jsonify({})


@app.route("/job/create", methods=["POST"])
def create():
    content = request.get_json(silent=True)
    job_id, object_id = content['job_id'], content["object_id"]
    create_job(job_id, object_id)
    logging.info(f"[x] Job {job_id}/{object_id} requested")

    return jsonify({"job_id": job_id, object_id: object_id})


@socketio.on('join')
def on_join(data):
    room = data['room']
    join_room(room)
    logging.info(f"[x] Joined {room}")
    socketio.send(redis.get_by_prefix(room, "*"), room=room, json=True)


@socketio.on('leave')
def on_leave(data):
    room = data['room']
    leave_room(room)


@socketio.on_error()
def error_handler(e):
    logging.warning("error:", e)


def list_objects(job_id):
    lst = redis.list(job_id)
    lst = list(filter(lambda x: x.startswith("files"), lst))
    lst = list(set(map(lambda x: x.split("/")[1], lst)))
    return lst


def cb():
    rabbit = Rabbit(host=RABBIT_HOST, port=RABBIT_PORT, username=RABBIT_USER, password=RABBIT_PASS)
    rabbit.listen("status", consumer(rabbit))


def parse_job_type(job_type):
    high = job_type.lower()
    if high in STATUS:
        return high
    return None


def extract_body(json):
    return json.get("jobId"), json.get("objectId", None), parse_job_type(json.get("jobType")), json.get(
        "status"), json.get("fileName", None)


def is_all_done(job_id):
    data = redis.get_by_prefix(job_id, "")
    logging.debug(data)

    if data["extract_complete"].lower() == "false":
        return False

    for k in data["files"]:
        v = data['files'][k]
        if v not in ["parsed", "failed"]:
            return False
    return True


def parse_complete(job_id, rabbit):
    val = update_main_status(job_id, "parse_completed", True)
    # logging.debug
    logging.debug("files to compress", list_objects(job_id))
    data = {
        "job_id": job_id,
        "files": list_objects(job_id)
    }
    rabbit.publish(data, "compress")
    logging.info("start compress")


def consumer(rabbit):
    def consumer_helper(_, __, ___, body):
        job_id, _, job_type, status, file_name = extract_body(body)
        logging.debug(job_type, job_id, status, file_name)
        if job_type is not None:

            if job_type == "parse":
                add_or_update_object_status(job_id, file_name, ("parsing" if not status else "parsed") if type(
                    status) == bool else "failed")
                is_done = is_all_done(job_id)

                if is_done:
                    logging.debug("done:", job_id)
                    parse_complete(job_id, rabbit)
                    socketio.send(redis.get_by_prefix(job_id, "*"), room=job_id, json=True)
                else:
                    # socketio.send(val, room=job_id, json=True)
                    socketio.send(redis.get_by_prefix(job_id, "*"), room=job_id, json=True)

            elif job_type == "extract":
                val = add_or_update_object_status(job_id, file_name, ("waiting" if not status else "extracted") if type(
                    status) == bool else "failed")
                # print(job_type, val)
                # socketio.send(val, room=job_id, json=True)
                socketio.send(redis.get_by_prefix(job_id, "*"), room=job_id, json=True)

            elif job_type in ["extract_complete", "compression_complete"]:
                val = update_main_status(job_id, job_type, status)
                # print(job_type, val)
                # socketio.send(val, room=job_id, json=True)
                socketio.send(redis.get_by_prefix(job_id, "*"), room=job_id, json=True)

            else:
                logging.warning("Invalid status:", job_type, job_id, status, file_name)

    return consumer_helper


def create_job(job_id, object_id):
    logging.info("new job", job_id, object_id)

    for elt in redis.list(job_id):
        redis.delete(job_id, elt)

    redis.put(job_id, {"extract_complete": False, "parse_complete": False, "compression_complete": False})

    json = {
        "jobId": job_id,
        "objectId": object_id
    }
    rabbitMain.publish(json, "extract")


def update_main_status(job_id, status, flag):
    if status in STATUS:
        redis.put(job_id, {status: flag})
        return {job_id: {status: flag}}

    return None


def add_or_update_object_status(job_id: str, filename: str, converted=None):
    if converted is None:
        converted = {"converted": False, "extracted": False}

    redis.put(job_id, {'files': {filename: converted}})
    return {job_id: {'files': {filename: converted}}}


if __name__ == '__main__':
    # app.run("0.0.0.0", debug=False, threaded=True)
    # with thread_lock:
    #     if thread is None:
    try:
        rabbitMain = Rabbit(host=RABBIT_HOST, port=RABBIT_PORT, username=RABBIT_USER, password=RABBIT_PASS)
        redis = Redis(host=REDIS_HOST, port=REDIS_PORT, password=REDIS_PASS)

        thread = socketio.start_background_task(target=cb)
        socketio.run(app, host="0.0.0.0", port=EXPOSE_PORT, debug=False)
    except Exception as e:
        logging.warning(e)
        sys.exit(2)
