pika==0.12
flask==1.0.2
flask-socketio==3.0.2
redis==2.10.6
gevent
gevent-websocket
flatten_json==0.1.6
gunicorn==19.9.0